# Aperçu

Dans ce projet nous allons utiliser terraform et ansible pour créer une infrastructure cloud dédiée à wordpress.

L'infrastructure as code rend l’administration de l’infrastructure IT plus facile, fini les processus manuels suceptible de générer des erreurs.

Cette pratique clé de DevOps permet d’effectuer des changements d’infrastructure plus facilement, rapidement et de manière plus fiable tout en étant répétables.

# Schéma d'architecture

![aws-wordpress-architecture](images/aws-wordpress-architecture.png)

Pour plus de scalabilité, la structure est construite sur deux zones de disponibilités.

Les serveurs web ne sont pas accessible au public, par contre ils peuvent accéder au net via la nat gateway.

La machine bastion joue le role de pont et permet d'y accéder aux instances privées via ssh.

L'application load balancer a pour but d'équilibrer la charge entre les serveurs web.

On trouve aussi les composantes suivant :

- **Amazon route 53:** service web amazon de système de noms de domaine (DNS).
- **Amazon cloudFront:** service destiné à la diffusion de contenu.
- **VPC:** Amazon Virtual Private Cloud, ce service permet de lancer des ressources AWS dans un réseau virtuel.
- **Internet gateway:** est un composant qui permet la communication entre votre VPC et Internet.
- **NAT gateway:** est un composant VPC qui permet aux instances du sous-réseau privé d'initier un trafic IPv4 sortant vers Internet.
- **MySQL RDS:** amazon Relational Database Service est un service web qui facilite la création et la mise à l'échelle d'une base de données relationnelle dans le Cloud AWS.
- **Elasticache memcached:** est un service qui permet de configurer et de mettre à l'échelle le cache de données en mémoire dans le cloud AWS.

# Prérequis

## Configuration de compte aws

Avant de commencer il faut créer un compte amazon aws, un compte free-tier fera l'affaire, mais il faut noter que l'utilisation de nat gateway est facturable.

Ajouter un utilisateur, et pour faire simple lui affecter tout les droits.

![USER MANAGEMENT](images/iam-create-user.png)

![USER MANAGEMENT](images/iam-create-user-2.png)

Récupérer la clé d'accès `ACCESS_KEY` et la clé d'accès secrète `SECRET_ACCESS_KEY`.

![USER MANAGEMENT](images/iam-create-user-3.png)

Installer aws cli (aws commande line).

```sh
$ sudo apt-get install awscli
````
Ajouter `ACCESS_KEY` et `SECRET_ACCESS_KEY` :

Soit manuellement en éditant le fichier  `~/.aws/credentials`

```ini
[default]
aws_access_key_id = la valeur de vote ACCESS_KEY
aws_secret_access_key = la valeur de vote SECRET_ACCESS_KEY
```
 
soit en tapant la commande :

```sh
$ aws configure
```

Au niveau de la console aws service EC2 créez votre Paire de clés (key pair).

![KEY PAIR](images/create-key-pair.png)

Remplissez le nom de la clé et choisissez le type pem, sauvegardez la par exemple dans le dossier 
`~/.ssh/` 

## Configuration terraform

Assurer que wget et unzip sont déjà installés.

```sh
$ sudo apt-get install wget unzip
```

Récupérer la dernière version de terraform.

```sh
$ VER=`curl -s https://api.github.com/repos/hashicorp/terraform/releases/latest | grep tag_name | cut -d: -f2 | tr -d \"\,\v | awk '{$1=$1};1'`
```
Télécharger l'archive terraform.

```sh
$ wget https://releases.hashicorp.com/terraform/${VER}/terraform_${TER_VER}_linux_amd64.zip
```
Une fois décompressée, déplacez le dossier vers `/usr/local/bin`

```sh
$ unzip terraform_${VER}_linux_amd64.zip
$ sudo mv terraform /usr/local/bin/
```
Pour vérifier que tout s'est bien déroulé, tappez la commande:

```sh
$ terraform -v
Terraform v0.12.26
````

## Configuration ansible

Installer ansible.

```sh
$ sudo apt install ansible`
```

Vérifiez que l'installation s'est bien déroulée

```sh
$ ansible --version`
```

vous deviez avoir une réponse comme la suivante :

```sh
ansible 2.9.6
 config file = /etc/ansible/ansible.cfg
 configured module search path = ['/home/rabie/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
 ansible python module location = /usr/lib/python3/dist-packages/ansible
 executable location = /usr/bin/ansible
 python version = 3.8.2 (default, Jul 16 2020, 14:00:26) [GCC 9.3.0]
```

Si ce n'est pas déjà fait, vous aurez besoin de créer une clé ssh public.

```sh
$ ssh-keygen
```

# Installer wordpress en utilisant terraform et ansible

le projet est devisé en deux parties :
- **aws-terraform:** permet de créer la structure aws comme décrite sur le schéma d'architecture.
- **ansible:** permet d'installer et configurer wordpress.


## AWS Terraform

Dans cette partie nous allons voir comment utiliser terraform pour créer les composants nécessaires pour notre projet.

### VPC

Tout d'abord, nous provisionnons le réseau virtuel VPC ainsi que les différentes subnets et group subnets. Nous aurons besoin de définir au préalable le provider.

```hcl
provider "aws" {
  region = var.aws_region
}
```
Dans le fichier [terraform.tfvars](aws-terraform/provider.tf), mettez à jour la valeur de votre amazon aws région. 

```hcl
# provision app vpc
resource "aws_vpc" "main_vpc" {
  cidr_block = var.vpc_cidr
  enable_dns_hostnames = true
  tags = {
    Name = "terraform-aws"
  }
}
```

[network.tf](aws-terraform/network.tf)

Pour le VPC nous allons utiliser le block d'adresse **`16.0.0.0/16`**.

Nous définissons pour chaque zone de disponibilité un sous-réseau public :

- **public_subnets:**
    - `16.0.0.0/24` pour la zone de disponibilité 1 `eu-west-2a`
    - `16.0.1.0/24` pour la zone de disponibilité 2 `eu-west-2b`

dans cette partie de réseau nous allons mettre en place l'instance bastion et les composants `internet gateway` `NAT gateway`.

Les serveurs webs et la base de données doivent être inaccessible au public, nous aurons besoin de créer les 4 sous-réseaux privés suivants :
- **private_app_subnets:** 
    - `16.0.100.0/24` pour la zone de disponibilité 1 `eu-west-2a`
    - `16.0.101.0/24` pour la zone de disponibilité 2 `eu-west-2b`

- **private_data_subnets:** 
    - `16.0.200.0/24` pour la zone de disponibilité 1 `eu-west-2a`
    - `16.0.201.0/24` pour la zone de disponibilité 2 `eu-west-2b`

Pour se connecter à internet de manière directe nous attachons une internet gateway à notre VPC.
Une connection unidirectionnelle vers l'extérieur s'impose pour avoir la possibilité de mettre à jour les serveurs webs, nous ajoutons alors une nat gateway pour chaque sous-réseau privé (private_app_subnets))

### Table de Routage 

Une fois la structure de réseau est posée, l'étape suivante est la définition des tables de routages : 

[routes.tf](aws-terraform/routes.tf)

- **table de routage public:** associée aux sous-réseaux publics `public_subnets`, tout le trafic est redirigé vers l'internet gateway.
- **table de routage privée:** associée aux sous-réseaux privés applicatif `private_app_subnets` , tout le trafic est redirigé vers la NAT gateway.

### Groupe de sécurité 

Pour organiser le trafic au sein de notre réseau, nous devons définir les règles de sécurités à travers le composant aws_security_group

[security-groups.tf](aws-terraform/security-groups.tf)

les contraintes réseaux à mettre en place pour :

**Instances web:**
- Autoriser l'accès HTTP entrant depuis l'application load balancer.
- Autoriser l'accès HTTPS entrant depuis l'application load balancer.
- Autoriser l'accès SSH entrant pour l'instance bastion.
- Autoriser le protocole icmp entrant depuis l'ensemble de VPC.
- Autoriser la sortie vers l'ensemble des adresses IPv4.

**Instance bastion:**
- Autoriser l'accès SSH entrant pour tout, mais ça sera plus judicieux de le faire que pour les ips des administrateurs.
- Autoriser le protocole icmp entrant pour tout.
- Autoriser la sortie vers l'ensemble des adresses IPv4.

**Base de données:**
- Autoriser le trafic entrant sur le port 3306 depuis les web serveurs.
- Autoriser le trafic entrant sur le port 3306 depuis l'instance bastion pour permettre d'effectuer les opérations de débogage ou maintenances.
- Autoriser le protocole icmp entrant depuis l'ensemble de VPC.

 
**Application load balancer:**
- Autoriser l'accès HTTP, HTTPS pour tout.
- Autoriser la sortie vers l'ensemble des adresses IPv4.

**Elasticache memcached:**
- Autoriser le trafic entrant sur le port 11211 depuis les web serveurs.
- Autoriser la sortie sur le port 11211 vers la base de données.

### Instances

#### Bastion

L'instance bastion est accessible via ssh, il permet de se connecter aux serveurs web et à la base de données.

Cette instance sera déployée sur le sous-réseau public de la première zone de disponibilité `eu-west-2a`, elle aura une ip public.

[bastion.tf](aws-terraform/bastion.tf)

```hcl
resource "aws_instance" "bastion" {
  ami = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"
  key_name = aws_key_pair.keypair.key_name
  vpc_security_group_ids = [aws_security_group.bastion_sg.id]
  subnet_id = element(aws_subnet.public_subnets.*.id, 0)
  availability_zone = element(var.availability_zones, 0)
  associate_public_ip_address = true

  tags = {
    Name = "terraform-aws-bastion"
  }
}
```

Nous utiliserons ici une instance de type `ubuntu` version 20.04 et ayant owner le `099720109477`. 

Cette methode permet de récupérer la dernière version de l'ami en question pour chaque nouvel déploiement.

[ami.tf](aws-terraform/ami.tf)

```hcl
data "aws_ami" "ubuntu" {
  most_recent = true
  filter {
    name = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["099720109477"]
}
```
Au niveau de bastion on génère automatiquement une configuration SSH afin de permettre à ansible se connecter aux serveurs web via SSH agent.

```hcl
locals {
  template_file_init = templatefile("${path.module}/templates/ssh.toml", {
    bastion_ip = aws_instance.bastion.public_ip,
    user = var.ansible_user,
    key_path = var.private_key
  })
}

resource "local_file" "ssh_conf" {
  content = local.template_file_init
  filename = "../ansible/ssh.cfg"
}
```

[ssh.toml](aws-terraform/templates/ssh.toml)
  
Enfin on affiche en sortie l'ip public de bastion.

```hcl
output "bastion_ip" {
  value = aws_instance.bastion.public_ip
}
 ```

#### MySQL RDS

La base de données est une aws MysSQL RDS de type `db.t2.micro` avec 10 go de stockage.
Elle sera déployée sur le sous-réseau privé `private_data_subnets`

[db-instances.tf](aws-terraform/db-instances.tf)

```hcl
resource "aws_db_instance" "mysql" {
  instance_class = "db.t2.micro"
  allocated_storage = 10
  availability_zone = var.availability_zones[0]
  engine = "mysql"
  engine_version = "5.7"
  storage_type = "gp2"
  name = var.db_name
  username = local.db_creds.db_username
  password = local.db_creds.db_password
  parameter_group_name = "default.mysql5.7"
  vpc_security_group_ids = [aws_security_group.db_sg.id]
  db_subnet_group_name = aws_db_subnet_group.mysql.name
  skip_final_snapshot = true
  tags = {
    Name = "terraform-aws-mysql"
  }
}
```
Les données de connection à la base de données doivent être chiffrées, nous utiliserons une clé KMS pour crypter ces données critiques.
Vous pouvez créer vote clé symétrique en utilisant le service aws key management (KMS).

![AWS KEY MANAGEMENT SERVICE](images/KMS.png)

Utilisez ensuite la commande aws :

```sh
$aws kms encrypt \                                                                                                                     130 ↵
  --key-id ************ \
  --region eu-west-2 \
  --plaintext fileb://db-creds.yml \
  --output text \
  --query CiphertextBlob \
  > db-creds.yml.encrypted
 ```
- **key-id=** est l'identifiant de la clé KMS crée précédemment.
- **db-creds.yml=** est le fichier contenant les données en claires
- **db-creds.yml.encrypted=** le nom de fichier crypté

La partie suivante permet les credentials depuis le fichier chiffré. 

```hcl
data "aws_kms_secrets" "db_credentials" {
  secret {
    name    = "db"
    payload = file("${path.module}/db-creds.yml.encrypted")
  }
}

locals {
  db_creds = yamldecode(data.aws_kms_secrets.db_credentials.plaintext["db"])
} 

resource "aws_db_instance" "mysql" {
  .
  .
  .
  username = local.db_creds.db_username
  password = local.db_creds.db_password
}
```

#### Elasticache memcached

Memcached est un système de cache objet distribué "in-memory", il permet d'accroître la vitesse de réponse des sites web.

Pour notre projet nous utiliserons le service Elasticache complétement géré par aws.

[elasticache.tf](aws-terraform/elasticache.tf)

```hcl
resource "aws_elasticache_cluster" "memcached_cluster" {
  cluster_id = "terraform-aws-cluster"
  engine = "memcached"
  engine_version = "1.5.16"
  maintenance_window   = "sun:05:00-sun:06:00"
  node_type = "cache.t2.micro"
  num_cache_nodes = length(var.private_data_subnet_cidrs)
  port = var.memcached_port
  subnet_group_name = aws_elasticache_subnet_group.memcached.name
  security_group_ids = [aws_security_group.memcached_sg.id]
  az_mode = "cross-az"
  tags = {
    Name = "terraform-aws-memcached"
  }
}
# create elasticache subnet group on the private data subnets
resource "aws_elasticache_subnet_group" "memcached" {
  description = "ElastiCache subnet group"
  name = "memcached-subnet-group"
  subnet_ids = aws_subnet.private_data_subnets.*.id
}
```
Le type de cache cluster est `cache.t2.micro` en deux noeuds (`length(var.private_data_subnet_cidrs`), il sera déployé sur le sous-réseau privé `private_data_subnets`.

Enfin, on affiche en sortie l'adresse du cluster déployé.

```hcl
output "memcached_endpoint_address" {
  value = aws_elasticache_cluster.memcached_cluster.cluster_address
}
```

#### Instances web serveurs

Les instances hébergeant wordpress sont de même type que l'instance bastion, des VM ubuntu 20.04.

[web-instances.tf](aws-terraform/web-instances.tf)

```hcl
resource "aws_instance" "web" {
  count = length(var.private_app_subnet_cidrs)
  ami = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"
  key_name = aws_key_pair.keypair.key_name
  vpc_security_group_ids = [aws_security_group.web_sg.id]
  subnet_id = element(aws_subnet.private_app_subnets.*.id, count.index)
  availability_zone = element(var.availability_zones, count.index)
  associate_public_ip_address = false
  tags = {
    Name = "terraform-aws-private-web-server-${count.index + 1}"
  }
  depends_on = [aws_instance.bastion, aws_db_instance.mysql]
}
```

Seule la machine bastion pourra accéder en SSH à ces instances, elles seront déployées dans le sous-réseau privé `private_app_subnets`.

Au moment de création, chaque instance lance une commande ansible pour dérouler la phase d'installation et configuration de wordpress.

```hcl
provisioner "local-exec" {
  command = <<EOT
    sleep 40;
    cd ../ansible;
    export ANSIBLE_HOST_KEY_CHECKING=False;
    ansible-playbook -u ${var.ansible_user} \
                     -i ${self.private_ip}, playbook.yml \
                     -e "wp_db_host=${aws_db_instance.mysql.address} \
                         wp_db_name=${var.db_name} \
                         wp_db_user=${local.db_creds.db_username} \
                         wp_db_password=${local.db_creds.db_password}"
  EOT
}
```
Les ips privés sont affichés en sortie :

```hcl
output "webserver1_ip" {
  value = aws_instance.web.*.public_ip[0]
}
output "webserver2_ip" {
  value = aws_instance.web.*.private_ip[1]
}
```

#### Application load balancer

[alb.tf](aws-terraform/alb.tf)

```hcl
resource "aws_alb" "alb" {
  name = "terraform-aws-alb"
  internal = false
  ip_address_type = "ipv4"
  load_balancer_type = "application"
  idle_timeout = 60
  security_groups = [
    aws_security_group.alb_sg.id
  ]
  subnets = aws_subnet.public_subnets.*.id
}
resource "aws_alb_target_group" "alb-target-group" {
  health_check {
    interval            = 10
    path                = "/"
    protocol            = "HTTP"
    timeout             = 5
    healthy_threshold   = 5
    unhealthy_threshold = 2
  }
  name        = "terraform-aws-alb-tg"
  port        = 80
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = aws_vpc.main_vpc.id
  stickiness {
    type = "lb_cookie"
    enabled = true
  }
  tags = {
    Name = "terraform-aws"
  }
}
resource "aws_alb_target_group_attachment" "alb-attachment-web" {
  count = length(aws_instance.web)
  target_group_arn = aws_alb_target_group.alb-target-group.arn
  target_id = element(aws_instance.web.*.id, count.index)
  port = 80
}
resource "aws_alb_listener" "http_listener" {
  load_balancer_arn = aws_alb.alb.arn
  port = 80
  protocol = "HTTP"
  default_action {
    type = "forward"
    target_group_arn = aws_alb_target_group.alb-target-group.arn
  }
}
```
Nous définissons un équilibreur de charge de type applicatif sur le sous-réseau public `public_subnets`, on lui affecte le groupe de sécurité `alb_sg` qui autorise les entrées http sur le port 80 depuis l'extérieur.

- **aws_alb_target_group:** il s'agit de la cible de load balancer, on y déclare aussi la configuration de health check et la manière dont il conservera la session entre les déférentes instances.

Dans notre cas on utilisera les `lb_cookie`.

- **aws_alb_target_group_attachment:** c'est ici que nous attribuons les instances EC2 à l'équilibreur de charge.

- **ws_alb_listener:** spécifie l'action que l'alb doit entreprendre, chaque demande sur le port 80 doit être transmise au groupe cible défini précédemment.

#### CloudFront distribution

CloudFront est un réseau mondial de diffusion de contenu, il permet de distribuer aux utilisateurs des données sans latence et à une vitesse de transfert élevée.

[cloudfront.tf](aws-terraform/cloudfront.tf)

D'abord on commence par déclarer l'origine de la distribution.

```hcl
origin {
  domain_name = aws_alb.alb.dns_name
  origin_id = aws_alb.alb.id
  custom_origin_config {
    http_port = 80
    https_port = 443
    origin_protocol_policy = "http-only"
    origin_ssl_protocols = ["TLSv1"]
  }
}
```
- **domain_name:** pointe vers l'équilibreur de charge crée au paravent.
- **origin_id:** un identifiant unique pour l'origine, ici on a utilisé l'id de l'équilibreur de charge.
- **origin_protocol_policy:** on spécifie que CloudFront utilisera juste le protocol HTTP pour se connecter à l'origine.

Ensuite, nous avons d'autres paramètres importants pour configurer la distribution :

```hcl
enabled = true
retain_on_delete = true
price_class = "PriceClass_100"
aliases = ["*.openclassrom.tk"]
```
- **enabled:** on spécifie que la distribution doit être activée.
- **retain_on_delete:** désactive la distribution pour simplifier sa suppression. 
Utile car les distributions CloudFront peuvent prendre plus de 15 minutes pour se propager.
- **price_class** : catégorie de tarif associée au prix maximum à payer pour le service cloudFront, 
nous choisissons la catégorie moins onéreuse `PriceClass_100` et qui inclut uniquement les régions (États-Unis, Canada et Europe).
- **aliases** : noms des domaines à partir desquels vous diffuserez votre site. Ceux-ci doivent correspondre aux enregistrements DNS.

L'étape suivante est la configuration de comportement de cache par défaut :

```hcl
default_cache_behavior {
  allowed_methods = [ "DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT" ]
  cached_methods = [ "GET", "HEAD" ]
  target_origin_id = aws_alb.alb.id
  forwarded_values {
    query_string = true
    headers = ["*"]
    cookies {
      forward = "all"
    }
  }
  viewer_protocol_policy = "allow-all"
  min_ttl = 0
  default_ttl = 3600
  max_ttl = 86400
}
``` 

- **allowed_methods:** les méthodes HTTP gérées par CloudFront et qui seront transférées vers le load balancer.
- **cached_methods:** les réponses des méthodes HTTP que CloudFront mettra en cache.
- **target_origin_id:** l'id de load balancer vers lequel CloudFront achemine le trafic. 
- **forwarded_values:** spécifie comment CloudFront gère les chaînes de requête, les cookies et les en-têtes HTTP.
- **viewer_protocol_policy:** le protocole à utiliser pour accéder aux données dans l'origine, il prend un de ces valeurs : `[allow-all, https-only, or redirect-to-https]` .
- **min_ttl:** durée maximum en second pendant laquelle les objets doivent rester dans le cache de la distribution.
- **max_ttl:** durée maximum en second pendant laquelle les objets doivent rester dans le cache de la distribution.
- **default_ttl:** durée par défaut en second pendant laquelle les objets doivent rester dans le cache de la distribution.

On configure maintenant CloudFront pour utiliser le certificat SSL liée à votre domaine :

```hcl
viewer_certificate {
  acm_certificate_arn = var.acm_certificate_arn
  ssl_support_method = "sni-only"
  minimum_protocol_version = "TLSv1.2_2019"
}
restrictions {
  geo_restriction {
    restriction_type = "none"
  }
}
```

- **acm_certificate_arn:** emplacement du certificat SSL défini par l'ARN (Amazon Resource Name). CloudFront prend uniquement en charge les certificats ACM dans la région (Virginie du Nord) (`us-east-1`).
- **ssl_support_method:** prend la valeur `sni-only`, ce que signifie que seuls les connections HTTPS qui supportent le SNI sont acceptées.
- **minimum_protocol_version:** version minimale du protocole SSL/TLS qui sera utilisé pour toute communication.
- **restriction_type:** prend la valeur `none`, aucune restriction géographique n'est imposée pour distribuer le contenus de site.

Vous pouvez réserver un domaine gratuit via le site [www.freenom.com/](https://www.freenom.com).

Utilisez le service amazon certificate manager (ACM) pur créer un certificat SSL pour votre domaine.

Notez bien que le certificat doit être créer au niveau de la zone de disponibilité `us-east-1`

![CERTIFICATE MANAGER](images/certificate-manager.png)
    
#### Route 53

[route53.tf](aws-terraform/route53.tf)

Amazon Route 53 est un service web qui gère le système de noms de domaine (DNS), il est hautement disponible et évolutif.

Nous allons créer une route 53 que nous attachons à notre distribution cloudFront crée précedement.

```hcl
resource "aws_route53_zone" "rt53_zone" {
  name = "openclassrom.tk"
  tags = {
    Name = "terraform-aws-rt53-zone"
  }
}
resource "aws_route53_record" "www" {
  zone_id = aws_route53_zone.rt53_zone.zone_id
  name = "*.openclassrom.tk"
  type = "A"
  alias {
    name = aws_cloudfront_distribution.cloudfront.domain_name
    zone_id = "Z2FDTNDATAQYW2"
    evaluate_target_health = false
  }
  depends_on = [aws_cloudfront_distribution.cloudfront]
}
```

- **alias.name:** fait référence au CloudFront distribution.
- **zone_id:** une valeur fixée en dure et utilisée pour toute les cloudFront distributions.

En sortie nous affichons la liste des names servers de la route.

```hcl
output "route_53_name_servers" {
  value = aws_route53_zone.rt53_zone.name_servers
}
```

## Ansible

Le projet ansible est organisé comme suivant :

![STRUCTURE PROJET ANSIBLE](images/ansible-project.png)

### group_vars :

[all.yml](ansible/group_vars/all.yml)

Ce dossier contient le fichier `all.yml`, c'est à cet endroit qu'on mentionne les variables globales qui seront utilisées par les playbooks.

### Playbook.yml :

[playbook.yml](ansible/playbook.yml)

playbook.yml est le fichier central pour la configuration d’Ansible, c'est le fichier qui est appelé par terraform au moment de la création des instances EC2 dédiées à wordpress.

Ce fichier lance la procédure d'installation en faisant appel à d'autres fichiers organisés par role.

```yaml
---
- name: Install Nginx, PHP-FPM and WordPress
  hosts: all
  gather_facts: False
  become: yes
  become_method: sudo
  roles:
    - common
    - nginx
    - php-fpm
    - wordpress
---
```

### roles:

#### common:

Ce role permet de mettre à jour le cache système.

[common task](ansible/roles/common/tasks/main.yml)

```yaml
---
- name: Update cache
  apt: name=aptitude update_cache=yes state=latest force_apt_get=yes
  tags: [ system ]
---
```

#### nginx:

[nginx task](ansible/roles/nginx/tasks/main.yml)

Permet d'installer et configurer le serveur nginx.

```yaml
---
- name: Install nginx
  apt:
    name: nginx
    state: latest
- name: Copy nginx configuration for wordpress
  template:
    src: "nginx.conf.j2"
    dest: "/etc/nginx/sites-available/{{ http_conf }}"
- name: Enables new site
  file:
    src: "/etc/nginx/sites-available/{{ http_conf }}"
    dest: "/etc/nginx/sites-enabled/{{ http_conf }}"
    state: link
  notify: restart nginx
- name: Removes "default" site
  file:
    path: "/etc/nginx/sites-enabled/default"
    state: absent
  notify: restart nginx
---
```

#### php-fpm:

[php-fpm task](ansible/roles/php-fpm/tasks/main.yml)

Installe les dépendances php pour le fonctionnement de wordpress.

```yaml
---
- name: Install php-fpm and dependencies
  apt:
    name: ['php-mysql', 'php-gd', 'php-intl', 'php-fpm', 'php-memcached']
    state: latest
---
```

#### wordpress:

[wordpress task](ansible/roles/wordpress/tasks/main.yml)

installe et configure wordpress 

```yaml
---
#WordPress Configuration
- name: Create document root
  file:
    path: "/var/www/{{ domain }}"
    state: directory
    owner: www-data
    group: www-data
    mode:  0755
  tags: [ wordpress ]

- name: Download and unpack latest WordPress
  unarchive:
    src: https://wordpress.org/latest.tar.gz
    dest: "/var/www/{{ domain }}"
    remote_src: yes
    owner: www-data
    group: www-data
    creates: "{{ wp_root }}"
  tags: [ wordpress ]

- name: Set up wp-config
  template:
    src: "wp-config.php.j2"
    dest: "{{ wp_root }}/wp-config.php"
    owner: www-data
    group: www-data
    mode: '0640'
  tags: [ wordpress ]

- name: Install plugins
  include_tasks: plugins.yml
  tags: [ wordpress ]

- name: Set permissions for directories
  shell: "/usr/bin/find {{ wp_root }} -type d -exec chmod 755 {} \\;"
  tags: [ wordpress ]

- name: Set permissions for files
  shell: "/usr/bin/find {{ wp_root }} -type f -exec chmod 644 {} \\;"
  tags: [ wordpress ]
---
```

On remarque aussi l'importation de tache plugins permettant d'installer le plugin memcached wp-ffpc.

D'abord, on commence par l'installation de wp-cli avec laquelle on configure le compte administrateur.

Nous utiliserons Ansible vault pour chiffrer le mot de passe administrateur. 

```sh
$ansible-vault encrypt_string 'your_admin_password' --name 'admin_password' --vault-password-file=.vault_pass
```

Le fichier .vault_pass contient votre propre mot pass vault.

[plugins task](ansible/roles/wordpress/tasks/plugins.yml)

```yaml
---
- name: Install WP-CLI
  get_url: url={{ wp_cli_url }} dest={{ wp_cli_bin }} mode=0755
  tags: [wordpress]

- name: Install wordpress site and create admin compte
  command: wp core install --url={{ site_url }} \
                           --title={{ site_title }} \
                           --admin_user={{ admin_user }} \
                           --admin_password={{ admin_password }} \
                           --admin_email={{ admin_email }} \
                           --path={{ wp_root }} \
                           --allow-root
  tags: [wordpress]

- name: Install WordPress plugins
  command: wp plugin install {{ item.name }} --version={{ item.version }} --activate --force --allow-root chdir={{ wp_root }}
  with_items: "{{ wp_plugins }}"
  when: wp_plugins is defined
  tags: [wordpress]
--- 
```

### Ansible configuration :

Nous ajoutons quelques lignes de configurations pour Ansible dans le fichier [ansible.cfg](ansible/ansible.cfg)

```ini
[defaults]
vault_password_file = .vault_pass
[ssh_connection]
ssh_args = -F ssh.cfg
control_path = ~/.ssh/mux-%r@%h:%p
```

- **vault_password_file:** le chemin vers le fichier vault password.
- **ssh_args:** paramètre à passer à Ansible SSH pour effectuer le rebond vers les serveurs wordpress.

### Procédure d'exécution :

D'abord récupérer le projet via git.

```sh
$ git clone https://gitlab.com/rsihammou/aws-terraform-ansible.git
```

Mettez à jour le fichier [terraform.tfvars](aws-terraform/terraform.tfvars) en remplaçant les variables suivantes :

- **acm_certificate_arn:** l'ARN de votre certificat SSL liée à votre domaine et à récupérer depuis votre console aws amazon.

Au niveau de fichier [vars.tf](aws-terraform/vars.tf) remplacez les valeurs des variables :

- **private_key:** chemin vers amazon aws key-pair
- **ssh_key:** chemin vers la clé ssh public.

Modifier le domaine et le mot de passe administrateur au niveau de fichier [all.yml](ansible/group_vars/all.yml)

Pour lancer automatiquement Ansible depuis Terraform, il vaut mieux désactiver la vérification stricte des clés RSA dans SSH.

```sh
sudo vim /etc/ssh/ssh_config 
```

Ajoutez la ligne `StrictHostKeyChecking no`.

à ce stade on est en mesure de lancer l'exécution de note infrastructure as code.

```sh
$ terraform init

$ terraform validate

$ terraform plan

$ terraform apply -auto-approve
```

Une fois terminée, on récupère les valeurs des names servers en sortie pour mettre à jour le domaine sur le site [www.freenom.com](https://www.freenom.com)

Onglet : services ->My Domains ->Manage Domain ->Manage Free DNS -> Edit Nameservers

![edit nameservers](images/edit-nameservers.png)

Connectez-vous à wordpress administration [your-domain/wp-admi](your-domain/wp-admin), ensuite choisissez la configuration des plugins.

![wordpress plugins](images/wordpress-plugins.png)

Ajoutez l'endpoint memcached récupéré en sortie terraform et validez la configuration.

![configuration plugin WP-FFPC](images/configuration-wp-ffpc.png)
resource "aws_key_pair" "keypair" {
  key_name = "tf-keypair"
  public_key = file(var.ssh_key)
}
resource "aws_route53_zone" "rt53_zone" {
  name = "openclassrom.tk"
  tags = {
    Name = "terraform-aws-rt53-zone"
  }
}

resource "aws_route53_record" "www" {
  zone_id = aws_route53_zone.rt53_zone.zone_id
  name = "*.openclassrom.tk"
  type = "A"

  alias {
    name = aws_cloudfront_distribution.cloudfront.domain_name
    zone_id = "Z2FDTNDATAQYW2"
    evaluate_target_health = false
  }
  depends_on = [aws_cloudfront_distribution.cloudfront]
}

output "route_53_name_servers" {
  value = aws_route53_zone.rt53_zone.name_servers
}


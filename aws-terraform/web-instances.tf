resource "aws_instance" "web" {
  count = length(var.private_app_subnet_cidrs)
  ami = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"
  key_name = aws_key_pair.keypair.key_name
  vpc_security_group_ids = [aws_security_group.web_sg.id]
  subnet_id = element(aws_subnet.private_app_subnets.*.id, count.index)
  availability_zone = element(var.availability_zones, count.index)
  associate_public_ip_address = false

  provisioner "local-exec" {
    command = <<EOT
      sleep 40;
      cd ../ansible;
      export ANSIBLE_HOST_KEY_CHECKING=False;
      ansible-playbook -u ${var.ansible_user} \
                       -i ${self.private_ip}, playbook.yml \
                       -e "wp_db_host=${aws_db_instance.mysql.address} \
                           wp_db_name=${var.db_name} \
                           wp_db_user=${local.db_creds.db_username} \
                           wp_db_password=${local.db_creds.db_password}"
    EOT
  }
  tags = {
    Name = "terraform-aws-private-web-server-${count.index + 1}"
  }
  depends_on = [aws_instance.bastion, aws_db_instance.mysql]

}

output "webserver1_ip" {
  value = aws_instance.web.*.private_ip[0]
}

output "webserver2_ip" {
  value = aws_instance.web.*.private_ip[1]
}

resource "aws_instance" "bastion" {
  ami = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"
  key_name = aws_key_pair.keypair.key_name
  vpc_security_group_ids = [aws_security_group.bastion_sg.id]
  subnet_id = element(aws_subnet.public_subnets.*.id, 0)
  availability_zone = element(var.availability_zones, 0)
  associate_public_ip_address = true

  tags = {
    Name = "terraform-aws-bastion"
  }
}

locals {
  template_file_init = templatefile("${path.module}/templates/ssh.toml", {
    bastion_ip = aws_instance.bastion.public_ip,
    user = var.ansible_user,
    key_path = var.private_key
  })
}

resource "local_file" "ssh_conf" {
  content = local.template_file_init
  filename = "../ansible/ssh.cfg"
}

output "bastion_ip" {
  value = aws_instance.bastion.public_ip
}


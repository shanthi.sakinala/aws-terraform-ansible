aws_region = "eu-west-2"
availability_zones = ["eu-west-2a", "eu-west-2b"]
public_subnet_cidrs = ["10.0.0.0/24", "10.0.1.0/24"]
private_app_subnet_cidrs = ["10.0.100.0/24", "10.0.101.0/24"]
private_data_subnet_cidrs = ["10.0.200.0/24", "10.0.201.0/24"]
db_name = "wordpress_db"
acm_certificate_arn = "arn:aws:acm:us-east-1:220056170837:certificate/9781c4e5-4cb0-4f6f-938e-ff01b0f0620b"
